$(document).ready(function () {
    $('#example').DataTable({
        "autoWidth": true,
        responsive: true,
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });


    $('#data').DataTable({
        "autoWidth": true,
        responsive: true,
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });

    // $.ajax({
    //     url:"../getdata",
    //     success:function (response) {
    //         console.log(response)
    //     }
    // });

});