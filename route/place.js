﻿let connection = require('../config/database');
let express = require('express');
let router = express.Router();
let bcrypt = require('bcrypt');
let checkData = require('./check');


router.get('/division', function(req, res) {


    let query = "select * from divisions order by name desc";

    connection.query(query, function(err, row) {
        if (err) throw err;

        res.render('pages/division', {
            data: row,
            active: 'Division',
            title: 'Division'
        });

    });

});

router.get('/addcity/:id',function(req,res){
	 let id = req.params.id;
     let division="";
     let query="select name from divisions where id =" + id;
     connection.query(query,function(err,row){
        // return res.json(query)
        if(err) throw err;

       res.render('pages/addcity',{
            active:"Division",
            id:id,
            division:row[0].name
        });
      

     });


});

router.get('/get/division/:id', function(req, res) {

    let id = req.params.id;
    let status = '';

    let query = " select * from cities where divisions_id = " + connection.escape(id);

    connection.query(query, function(err, row) {

        if (err) throw err;

        return res.json({row});

    });

});


router.post('/add/newcity',function(req,res){

    let data=JSON.parse(req.body.data);

    let city=data.city;
    let division=data.division_id;

    let query="insert into cities (divisions_id,city) values ("+connection.escape(division)+ "," +connection.escape(city)+")";

    connection.query(query,function(err,result){
        if(err) return res.json(err);

        return res.json({data:1});
    });

});


router.get('/edit/city/:id',function(req,res){
    let id=req.params.id;
    let query="select city from cities where id = " + connection.escape(id);

    connection.query(query,function(err,result){

        if(err) throw err;

        return res.json({result});

    });

});



router.post('/edit/city',function(req,res){

    let data=JSON.parse(req.body.data);

    let id=data.id;
    let city=data.city;

    let query="update cities set city =" + connection.escape(city) + " where id =  " + connection.escape(id);

    connection.query(query,function(err,result){
        if(err) return res.json(err);

        return res.json({data:1});
    });

});

router.get('/delete/city/:id',function(req,res){
    let id=req.params.id;
    let query="delete from cities where id = " + connection.escape(id);
    connection.query(query,function(err,result){
        if(err) throw err;
          return res.json({data:1});

    });
});


router.post('/search',function(req,res){
     let data=JSON.parse(req.body.data);

     let division=data.division_id;


    let city=data.city;

    let query="select * from cities where divisions_id = ? and city like ? " ;

     // return res.json(query);


    connection.query(query,[division,'%'+city+'%'],function(err,row){
        if(err) return res.json(err);

        return res.json({row});
    });
});

module.exports = router;
