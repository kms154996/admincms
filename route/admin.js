let connection = require('../config/database');
let express = require('express');
let router = express.Router();
let bcrypt = require('bcrypt');
let checkData = require('./check');
// let cookieParser = require('cookie-parser');
const {check, validationResult} = require('express-validator/check');

let async = require('async');

let result = [];
let data = [];

router.get('/', function (req, res) {
    res.render('pages/login', {
        data: {},
        errors: {}
    });
});

router.post('/login', function (req, res) {

    let name = req.body.name.toLowerCase();
    let password = req.body.password;

    let query = "select * from users where username = " + connection.escape(name) + " limit 1";

    connection.query(query, function (err, row) {
        if (err) console.log(err);

        if (checkData.isEmptyObject(row)) {
            req.flash('success', 'Username is wrong');
            res.redirect('/');
        } else {
            if (!bcrypt.compareSync(password, row[0].password)) {
                req.flash('success', 'Password is wrong');
                res.redirect('/');
            } else {
                req.session.user = row;
                res.redirect('/data');
            }
        }

    });

});

router.get('/logout', (req, res) => {

    res.redirect('/');

});

router.get('/data', function (req, res) {

    let url = req.protocol + '://' + req.get('host');

    connection.query("select count(*) as users from users where permission = 0 and month(registered_date) = month(Now()) ; SELECT count(*) as schools FROM schools where month(created_date) = month(Now()) ;" +
        " select count(*) as students from students  where  month(created_at) = month(Now());" +
        "select * from users where  school_creation != 0 or tution !=0", function (err, results) {
        if (err) return res.json(err);

        console.log(JSON.stringify(results[3][0]));

        res.render('pages/index', {
            active: 'index',
            user_count: results[0][0].users,
            school_count: results[1][0].schools,
            student_count: results[2][0].students,
            data: results[3],
            url: url
        });

    });


});

router.get('/userdetail/:id', function (req, res) {

    let id = req.params.id;

    let query = "select *,divisions.name as division,cities.city as ciity from users,cities,divisions where   users.id =" + connection.escape(id) + " and cities.id = users.city and users.division = divisions.id  ";

    connection.query(query, function (err, row) {
        if (err) return res.json(err);

        // return res.json(row);

        if (checkData.isEmptyObject(row)) {
            res.render('pages/404', {
                active: '404'
            });
        } else {
            res.render('pages/user_detail', {
                data: row[0],
                active: 'index'
            });
        }
    });


});

router.get('/users', function (req, res) {
    let query = "select * from users ";
    connection.query(query, function (err, row) {
        if (err) throw err;

        if (checkData.isEmptyObject(row)) {
            res.render('pages/404');
        } else {
            res.render('pages/users', {
                data: row,
                active: 'Users'
            })
        }

    });
});

router.get('/getdata', function (req, res) {


    let query = " select count(*) as total,concat(year(registered_date),'-',month(registered_date)) as date from users group by concat(year(registered_date),'-',month(registered_date)) ;" +
        "select count(*) as schools,concat(year(created_date),'-',month(created_date)) as date from schools where permission in (0,1) group by concat(year(created_date),'-',month(created_date));" +
        "select count(*) as students,concat(year(created_at),'-',month(created_at)) as date from students group by concat(year(created_at),'-',month(created_at));";


    connection.query(query, function (err, results) {
        if (err) return res.json(err);

        // console.log(results[2]);
        // return res.json(results[0]);

        data = joinObjects(results[0], results[1], results[2]);

        return res.json(data);


    });

});


function joinObjects() {
    var idMap = {};
    // Iterate over arguments
    for (var i = 0; i < arguments.length; i++) {
        // Iterate over individual argument arrays (aka json1, json2)
        for (var j = 0; j < arguments[i].length; j++) {
            var currentID = arguments[i][j]['date'];
            if (!idMap[currentID]) {
                idMap[currentID] = {"schools": 0, "total": 0, "students": 0};
            }
            // Iterate over properties of objects in arrays (aka id, name, etc.)
            for (key in arguments[i][j]) {
                idMap[currentID][key] = arguments[i][j][key];
            }
        }
    }

    // push properties of idMap into an array
    var newArray = [];
    for (property in idMap) {
        newArray.push(idMap[property]);
    }
    return newArray;
}

module.exports = router;