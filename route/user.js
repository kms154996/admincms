let connection = require('../config/database');
let express = require('express');
let router = express.Router();
let bcrypt = require('bcrypt');
let checkData = require('./check');


router.get('/schools', function (req, res) {


    let query = "select * from users where school_creation in (1,2) and permission = 0";

    connection.query(query, function (err, rows) {

        if (err) throw err;

        if (checkData.isEmptyObject(rows)) {
            res.render('pages/404',{
                active:'404'
            });
        } else {
            res.render('pages/school', {
                data: rows,
                active: 'Users'
            });
        }

    });
});


router.get('/school/:id',function (req,res) {

    let id=req.params.id;

    let query="select * from schools where user_id = " + connection.escape(id) + " order by created_date desc " ;

    connection.query(query,function (err,rows) {

        if(err) throw  err;

        // return res.json(rows);

        if (checkData.isEmptyObject(rows)) {
            res.render('pages/404',{
                active:'404'
            });
        } else {
            res.render('pages/user_schools', {
                data: rows,
                active: 'School'
            });
        }

    });

});

module.exports = router;


