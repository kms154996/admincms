let connection = require('../config/database');
let express = require('express');
let router = express.Router();
let bcrypt = require('bcrypt');
let checkData = require('./check');

let admin = require('../config/noti');

router.get('/edit/:id', function (req, res) {

    let id = req.params.id;

    let query = "select * from schools where id =" + connection.escape(id);

    connection.query(query, function (err, row) {

        if (err) throw err;

        if (checkData.isEmptyObject(row)) {
            res.render('pages/404', {
                active: '404'
            });
        } else {
            res.render('pages/school_edit', {
                data: row,
                active: 'School',
                title: 'School'
            });
        }


    });

});


router.get('/edit/:id/:change', function (req, res) {
    let id = req.params.id;
    let change = req.params.change;
    // let execute_query=null;

    if (change == 1) {
        let query = "update schools set expired_date = DATE_ADD(created_date,INTERVAL 1 YEAR ) , permission = " + connection.escape(change) + " where id = " + connection.escape(id);

        // return res.json(query);
        connection.query(query, function (err, result) {
            if(err) return res.json(err);
            if (err) return res.json({status: 0, "message": "Cannot Update"});
            sendNoti(id);
            return res.json({status: 1, "message": "Update Success", "change": change})

        });
    } else if (change == 2 || change == 3) {
        let query = "update schools set permission = " + connection.escape(change) + " where id = " + connection.escape(id);
        connection.query(query, function (err, result) {
            if (err) return res.json({status: 0, "message": "Cannot Update"});
            sendNoti(id);
            return res.json({status: 1, "message": "Update Success", "change": change})

        });
    }

});

function sendNoti(id) {
    let query = " select users.device_token,schools.permission from users,schools where schools.id =  " + connection.escape(id) + " and schools.user_id = users.id ";

    connection.query(query, function (err, result) {
        if (err) return res.json(err);

        let status = result[0].permission;
        let device_token = result[0].device_token;
        let message = '';

        if (status === 1) {
            message = 'Your school open';
        } else if (status === 2) {
            message = 'Your school expired'
        } else if (status === 3) {
            message = 'Your school closed';
        } else if (status === 0) {
            return res.json({status: 1, "message": "Update Success"})
        }

        sendMessage(message, device_token);

    });
}

router.get('/all', function (req, res) {

    let query = "select * from schools order by id desc";

    connection.query(query, function (err, rows) {
        if (err) res.render('pages/503');

        // return res.json(rows);

        res.render('pages/schools', {
            active: "School",
            data: rows
        });

    });

});

router.get('/getschoolbymonth', function (req, res) {
    let query = "select count(*) as schools,concat(year(created_date),'-',month(created_date)) as date from schools where permission in (0,1) group by concat(year(created_date),'-',month(created_date));" +
        "select count(*) as openschools,concat(year(created_date),'-',month(created_date)) as date from schools where permission = 1 group by concat(year(created_date),'-',month(created_date));";

    connection.query(query, function (err, results) {
        if (err) return res.json(err);

        let data = joinObjects(results[0], results[1]);
        return res.json(data);

    });
});


function joinObjects() {
    let idMap = {};
    // Iterate over arguments
    for (let i = 0; i < arguments.length; i++) {
        // Iterate over individual argument arrays (aka json1, json2)
        for (let j = 0; j < arguments[i].length; j++) {
            let currentID = arguments[i][j]['date'];
            if (!idMap[currentID]) {
                idMap[currentID] = {"schools": 0, "openschools": 0};
            }
            // Iterate over properties of objects in arrays (aka id, name, etc.)
            for (key in arguments[i][j]) {
                idMap[currentID][key] = arguments[i][j][key];
            }
        }
    }

    // push properties of idMap into an array
    let newArray = [];
    for (property in idMap) {
        newArray.push(idMap[property]);
    }
    return newArray;
}


function sendMessage(message, device_token) {
    var payload = {
        notification: {
            title: "Change School Noti",
            body: message
        }
    };
    admin.messaging().sendToDevice(device_token, payload)
        .then(function (response) {
            console.log("Successfully sent message:", response);
        })
        .catch(function (error) {
            console.log("Error sending message:", error);
        });
}

module.exports = router