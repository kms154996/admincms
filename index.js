let express = require('express');
let app = express();
const flash = require('express-flash');
// let cookieParser = require('cookie-parser');
let toastr = require('express-toastr');
let errorHandler = require('errorhandler');

let bodyParser = require('body-parser');
const session = require('express-session');

const validator = require('express-validator');

let connection = require('./config/database');

let path=require('path');

connection.connect(function (err) {
    if (!err) {
        console.log('Database Connect');
    } else {
        console.log('Cannot Connect');
    }
});

const middleware = [
    validator(),
    bodyParser.urlencoded({extended: true}),
    session({
        secret: 'super-secret-key',
        key: 'super-secret-cookie',
        resave: false,
        saveUninitialized: false,
        cookie: {maxAge: 60000}
    }),
    flash(),
    toastr(),
    // cookieParser()
];

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use(nocache);
app.use(errorHandler({
    dumpExceptions: true,
    showStack: true
}));

function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}

app.use(middleware);

app.use(bodyParser.json({type: 'application/json'}));

app.use(express.static(__dirname+'/public'));




app.set('view engine', 'ejs');   


let admin = require('./route/admin');
let user= require('./route/user');
let school=require('./route/school');
let place=require('./route/place');

app.use('/', admin);

// app.use(sessionChecker);

app.use('/user',user);
app.use('/school',school);
app.use('/place',place);





app.listen(process.env.PORT || 4000 );
